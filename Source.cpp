/*
		HW_2 Computer Graphics
		Ali Seraji 
		9531282
*/

#include<iostream>
#include<string>
#include<vector>
#include<cmath>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "lodepng.h"


using namespace std;
using  namespace glm;


class SaveImage {
private:
	int width, height;
	string fName;
public:
	SaveImage(string fName,int width,int height):fName(fName),width(width),height(height) {}
	void createImageFile(unsigned char* image) {
		vector<unsigned char>png;
		unsigned error = lodepng::encode(png, image, width, height);
		if (!error)
			lodepng::save_file(png,fName);
		cout << "Image is created!" << endl << "Press any key to continue." << endl;
	}
};

class CreateImage {
private:
	int width;
	int height;
public:
	unsigned char* buffer;
	CreateImage(int width, int height) {
		this->width = width;
		this->height = height;
		buffer = new unsigned char[width * height * 4];
	}

	int getWidth() {
		return width;
	}

	int getHeight() {
		return height;
	}

	void setPixel(int i, int j, vec3 color,float cos) {
		
		if (cos > 180)cos -= 180;

		unsigned char r = static_cast<unsigned char>(color.r);
		unsigned char g = static_cast<unsigned char>(color.g);
		unsigned char b = static_cast<unsigned char>(color.b);

		buffer[i * width * 4 + 4 * j + 0] = r*cos;
		buffer[i * width * 4 + 4 * j + 1] = g*cos;
		buffer[i * width * 4 + 4 * j + 2] = b*cos;
		buffer[i * width * 4 + 4 * j + 3] = 255;
	}
};

class Ray {
private:
	vec3 origin;
	vec3 direction;
public:
	Ray(vec3 origin, vec3 direction) {
		this->origin = origin;
		this->direction = direction;
	}
	vec3 getOrigin() {
		return origin;
	}
	vec3 getDirection() {
		return direction;
	}
	vec3 point_at_parameter(float t) {
		return (origin + (t*direction));
	}
};

class Hitable {
public:
	virtual bool intersect_ray(Ray& ray, vec3* intersectPosition,
		vec3* intersectNormal, float* T, vec3* col) = 0;
};

class Plane : public Hitable { 
public:
	vec3 normal;
	float c;
	vec3 color;
	Plane(vec3 normal, float c, vec3 color):normal(normal), c(c), color(color) {}
	bool intersect_ray(Ray& ray, vec3* intersectPosition, vec3* intersectNormal, float* T, vec3* col) {
		
		float denom = dot(normal, ray.getDirection());
		
		if (abs(denom) < 0.00001f) return false;

		float t = (c - dot(normal, ray.getOrigin())) / denom;
		if (t < 0) return false;

		if (intersectPosition != nullptr) {
			*intersectPosition = ray.point_at_parameter(t);
		}
		if (T != nullptr) {
			*T = t;
		}
		if (intersectNormal != nullptr) {
			*intersectNormal = normal;
		}
		if (col != nullptr) {
			*col= color;
		}
		return true;
	}
};

class Camera {
public:
	CreateImage image;
	float fov;
	float world_space_min_y;
	float world_space_height;

	float world_space_min_x;
	float world_space_width;

	Camera(int width, int height, float fov) :image(width, height), fov(fov) {
		world_space_min_y = -tanf(fov / 2);
		world_space_height = 2 * tanf(fov / 2);
		float aspect_ratio = static_cast<float>(image.getWidth()) / image.getHeight();
		world_space_min_x = aspect_ratio * world_space_min_y;
		world_space_width = aspect_ratio * world_space_height;
	}

	Ray get_pixel_ray(int i, int j) {
		float x_ratio = static_cast<float>(i) / image.getWidth();
		float y_ratio = static_cast<float>(j) / image.getHeight();
		float worldspace_x = world_space_min_x + x_ratio * world_space_width;
		float worldspace_y = world_space_min_y + y_ratio * world_space_height;
		return Ray(vec3(0,0,0), normalize(vec3(worldspace_x, worldspace_y, -1)));
	}

};

bool solve_quadratic_quation(float a, float b, float c, float& x1, float& x2) {
	float delta = (b * b) - (4 * a * c);
	if (delta < 0) return false;
	x1 = (-b + sqrt(delta))/(2 * a);
	x2 = (-b - sqrt(delta))/(2 * a);
	return true;
}

class Sphere : public Hitable {
public:
	float r;
	vec3 origin;
	vec3 color;
	Sphere(vec3 origin, float r, vec3 color) : origin(origin), r(r), color(color) {}

	virtual bool intersect_ray(Ray& ray, vec3* intersectPosition, vec3* intersectNormal, float* T, vec3* col) override{
		vec3 oc = ray.getOrigin()-origin;
		float a = dot(ray.getDirection(), ray.getDirection());
		float b = 2.0*dot(oc, ray.getDirection());
		float c = dot(oc, oc)-(r*r);
		float x1, x2;

		if (!solve_quadratic_quation(a, b, c, x1, x2)) return false;

		if (x2 < 0) return false;
		
		if (T != nullptr) {
			*T= x2;
		}

		vec3 intersect_position = ray.point_at_parameter(x2);
		if (intersectPosition != nullptr) {
			*intersectPosition = ray.point_at_parameter(x2);
		}
		if (intersectNormal != nullptr) {
			*intersectNormal = normalize(intersect_position - origin);
		}

		if (col!= nullptr) {
			*col= color;
		}

		return true;
	}
};

class Triangle :public Hitable {
private:
	vec3 v0, v1, v2,color;

public:
	Triangle(vec3 v0, vec3 v1,vec3 v2,vec3 color):v0(v0),v1(v1),v2(v2),color(color) {}

	virtual bool intersect_ray(Ray& ray, vec3* intersectPosition, vec3* intersectNormal, float* T, vec3* col) override{
		vec3 v0v1 = v1 - v0;
		vec3 v0v2 = v2 - v0;
		
		vec3 N =cross(v0v1,v0v2); 
		float area = length(N)/2;
		float NRay = dot(N,ray.getDirection());
		float d = dot(N,v0);

		float t = (dot(N,ray.getOrigin()) + d) / NRay;
		
		vec3 P = ray.point_at_parameter(t);
		vec3 C; 

		vec3 e0 = v1 - v0;
		vec3 vp0 = P - v0;
		C = cross(e0,vp0);
		if (dot(N,C) < 0) return false; // P is out of triangle 

		
		vec3 e1 = v2 - v1;
		vec3 vp1 = P - v1;
		C = cross(e1,vp1);
		if (dot(N,C) < 0)  return false; //P is out of triangle

		
		vec3 e2 = v0 - v2;
		vec3 vp2 = P - v2;
		C = cross(e2,vp2);
		if (dot(N,C) < 0) return false; // P is out of triangle 
		
		if (intersectNormal != nullptr) {
			*intersectNormal = normalize(P);
		}
		
		if (intersectPosition != nullptr) {
			*intersectPosition = P;
		}

		if (col != nullptr) {
			*col= color;
		}

		if (T != nullptr) {
			*T= t;
		}
		return true;
	}
};

class DrirectionalLight {
public:
	DrirectionalLight(){}
	float shineLight(vec3 normal,vec3 origin) {
		float numerator = length(dot(normal,origin));
		float denom = length(normal) * length(origin);
		return numerator / denom;
	}
};

int main() {

	float cos=0;
	DrirectionalLight light;
	string fileName = "ray.png";
	
	Camera cam(500, 500,(float)radians(90.0));
	
	Plane plane(vec3(1, 0, 0), -1, vec3(165, 3, 252.0));
	Plane plane2(vec3(1, 0, 0), 1, vec3(52.0, 103.0, 235.0));
	
	Sphere sphere(vec3(0,0,-5), 1, vec3(186, 252, 3));
	Sphere sphere1(vec3(0, 6, -10), 0.5, vec3(186, 252, 3));
	Sphere sphere2(vec3(0, -6, -10), 0.5, vec3(186, 252, 3));

	Triangle tri(vec3(2,2,-2),vec3(0,1,-3),vec3(2,1,-1),vec3(0.0,255.0,0.0));
	//Triangle tri1(vec3(-1, -2, -2), vec3(-1, 1, -3), vec3(-2, -1, -1), vec3(0.0, 255.0, 0.0));

	vector<Hitable*> v;
	
	v.push_back(&sphere);
	v.push_back(&plane);
	v.push_back(&sphere1);
	v.push_back(&sphere2);
	v.push_back(&plane2);
	v.push_back(&tri);
	//v.push_back(&tri1);
	
	
	for (int i = 0; i < cam.image.getWidth(); i++) {
		for (int j = 0; j < cam.image.getHeight(); j++) {
			Ray ray = cam.get_pixel_ray(i, j);
			vec3 min_color(0.0f, 0.0f, 0.0f);
			vec3 min_position;
			vec3 min_normal(0,0,0);
			float minT = 1000000.0f;
			for (Hitable* h : v) {
				vec3 color;
				vec3 position;
				vec3 normal;
				float t;
				if ((h->intersect_ray(ray, &position, &normal, &t, &color))) {
					if (t < minT) {
						minT = t;
						min_position = position;
						min_color = color;
						min_normal = normal;
					}
				}
				cos=light.shineLight(-min_normal, ray.getDirection());
			}
			cam.image.setPixel(i, j, min_color,cos);
		}
	}
	SaveImage saveImage(fileName,cam.image.getWidth(),cam.image.getHeight());
	saveImage.createImageFile(cam.image.buffer);
}













